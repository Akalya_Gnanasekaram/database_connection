package bcas.adp.dbs;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JDBC_Connector {
	public static Connection getConnection() throws ClassNotFoundException, SQLException {	
		Connection connection = null;		
		Class.forName(DBConstants.DRIVER);	
		String Connection_string="JDBC:mysql://" + DBConstants.HOST + ":" + DBConstants.PORT+ "/"+ DBConstants.DB_NAME;	
		System.out.println("Connection_string : ---->>>>" + Connection_string);	
		connection = DriverManager.getConnection(Connection_string,DBConstants.DB_USER_NAME,DBConstants.DB_PASSWORD);	
		return connection;	}
}