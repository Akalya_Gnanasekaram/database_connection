package bcas.ap.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JDBCConnection1 {

	public static void main(String[] args) {
		System.out.println("...........MYSQL JDBC Connection Test........");

		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			System.out.println("JDBC driver is attached");
		} catch (ClassNotFoundException e) {
			System.err.println("JDBC driver is not attached");
			e.printStackTrace();

		}

		Connection connection = null;
		try {
			connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/csd16", "root", "root");
		} catch (SQLException e) {

			e.printStackTrace();
		}

		if (connection != null) {
			System.out.println("Database successful Connected!!");
		} else {
			System.out.println("failed Connected!!");
		}

	}

}
